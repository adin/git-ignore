I'm not maintaining this repo anymore.  The git ignore file now lives within my [dotfiles](https://gitlab.com/adin/dotfiles) repo.

## About

A global configuration file for `git`. It removes common files
- C++
- C
- CMake
- Python
- TeX
- Matlab
- Temporals (files from Unix)

## Usage
Set this file using

```shell
git config --global core.excludesfile ~/.gitignore_global
```

Note that if you cloned this repository on another directory it will be wise to do a soft link to the `~/.gitignore_global` file or configure `git` to point to the cloned path.

## More

I got the original ignore rules from https://github.com/github/gitignore. There are more languages and tools there.